#!/bin/bash
# installs the usb_protector as a systemctl service
set -e

DATA_PATH='/etc/usb_protector'
VENV_PATH=$DATA_PATH'/venv'
DAEMON_BIN_PATH='/usr/sbin/usb_protector_daemon'
GUI_BIN_PATH='/usr/bin/usb_protector_gui'
NOTIF_BIN_PATH='/usr/bin/usb_protector_notification_daemon'
DAEMON_SYSTEMD_PATH='/usr/lib/systemd/system/usb_protector.service'
NOTIF_SYSTEMD_PATH='/usr/lib/systemd/user/usb_protector_notification_daemon.service'

# install venv
sudo mkdir -p $DATA_PATH
sudo chmod 0777 $DATA_PATH
/usr/bin/env python3 -m virtualenv -p /usr/bin/python3 --system-site-packages $VENV_PATH
. $VENV_PATH'/bin/activate'
sudo /usr/bin/env pip3 install -r requirements.txt

# install daemon
sudo cp src/usb_protector_daemon.py $DAEMON_BIN_PATH
sudo chmod 0755 $DAEMON_BIN_PATH
sudo chown root:root $DAEMON_BIN_PATH

# install gui
sudo cp src/usb_protector_gui.py $GUI_BIN_PATH
sudo chmod 0755 $GUI_BIN_PATH
sudo chown root:root $GUI_BIN_PATH

# install notification daemon
sudo cp src/usb_protector_notification_daemon.py $NOTIF_BIN_PATH
sudo chmod 0755 $NOTIF_BIN_PATH
sudo chown root:root $NOTIF_BIN_PATH

# copy config
sudo cp data/*.yml $DATA_PATH
sudo chmod 0666 $DATA_PATH/*.yml

# copy ui to data dir
sudo mkdir -p $DATA_PATH'/ui'
sudo chmod 0777 $DATA_PATH'/ui'
sudo chown root:root $DATA_PATH'/ui'
cp data/ui/*.ui $DATA_PATH'/ui'
chmod 0644 $DATA_PATH/ui/*.ui

# copy translations to data dir
cp -r data/locale $DATA_PATH'/locale'

# create desktop file
sudo cp data/usb_protector_gui.desktop '/usr/share/applications/usb_protector_gui.desktop'
sudo chmod o+r '/usr/share/applications/usb_protector_gui.desktop'

# replace shebang for daemon
sudo sed -i 's%#!/usr/bin/env python3%#!'$VENV_PATH'/bin/python3%g' $DAEMON_BIN_PATH
# replace shebang in gui
sudo sed -i 's%#!/usr/bin/env python3%#!'$VENV_PATH'/bin/python3%g' $GUI_BIN_PATH
# replace shebang in notification daemon
sudo sed -i 's%#!/usr/bin/env python3%#!'$VENV_PATH'/bin/python3%g' $NOTIF_BIN_PATH

# replace path to localedir for gui
sudo sed -i "s%LOCALE_PATH = os.path.join(os.path.dirname(__file__), '..', 'data', 'locale')%LOCALE_PATH = '$DATA_PATH/locale'%g" $GUI_BIN_PATH
# replace path to localedir for notification daemon
sudo sed -i "s%LOCALE_PATH = os.path.join(os.path.dirname(__file__), '..', 'data', 'locale')%LOCALE_PATH = '$DATA_PATH/locale'%g" $NOTIF_BIN_PATH
# replace path to ui
sudo sed -i "s%add_from_file(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', 'data',%add_from_file(os.path.join('$DATA_PATH',%g" $GUI_BIN_PATH


# install systemd service
sudo cp data/usb_protector.service $DAEMON_SYSTEMD_PATH
sudo chmod 0644 $DAEMON_SYSTEMD_PATH
sudo chown root:root $DAEMON_SYSTEMD_PATH
sudo systemctl enable usb_protector.service
sleep 1
sudo systemctl start usb_protector.service

# autostart notification service
sudo cp data/usb_protector_notification_daemon.desktop /etc/xdg/autostart/usb_protector_notification_daemon.desktop
sudo chmod 0644 /etc/xdg/autostart/usb_protector_notification_daemon.desktop

# start notification daemon
$NOTIF_BIN_PATH > /dev/null &

echo "Successfully installed USB Protector"
