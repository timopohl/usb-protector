#!/bin/sh

prospector --profile .prospector.yml main.py
mypy --config .mypy.ini main.py
