#!/bin/bash

LOCALEDIR="data/locale"
LOCALES=("de_DE")
ALL_FILES=("src/usb_protector_gui.py" "src/usb_protector_notification_daemon.py")

copy_po () {
  # $1: file name without path or extension
  # $2: language as specified in $LOCALES
  mkdir -p $LOCALEDIR/$2/LC_MESSAGES
  cp $1.pot $LOCALEDIR/$2/LC_MESSAGES/$1.po
}

update_po () {
  # $1: file name without path or extension
  # $2: language as specified in $LOCALES
  msgmerge --update $LOCALEDIR/$2/LC_MESSAGES/$1.po $1.pot
  touch -m $LOCALEDIR/$2/LC_MESSAGES/$1.po
}

remove_mo () {
  # $1: file name without path or extension
  # $2: language as specified in $LOCALES
  rm $LOCALEDIR/$2/LC_MESSAGES/$1.mo 2>/dev/null
}

case "$1" in
  "autogen")
    # generates all .po files that dont exist
    # updates all .po files that are out-of-date
    # removes all .mo files for .po files that got newly generated or updated
    NEW_OR_UPDATED=0
    for file in ${ALL_FILES[@]}
    do
      _filename="$(basename $file)"
      _fname=${_filename%.*}
      echo "processing $_fname"
      xgettext -d $_fname -o $_fname.pot $file
      for lang in ${LOCALES[@]}
      do
        if [ -f $LOCALEDIR/$lang/LC_MESSAGES/$_fname.po ]; then
          # .po file exists
          if [ $file -nt $LOCALEDIR/$lang/LC_MESSAGES/$_fname.po ]; then
            # .py file is newer than .po file -> update .po file
            update_po $_fname $lang
            echo "updated $_fname.po for $lang"
            NEW_OR_UPDATED=1
            # remove now outdated .mo file
            remove_mo $_fname $lang
          else
            # .py file is NOT newer than .po file -> dont update .po file
            echo "locale/$lang/LC_MESSAGES/$_fname.po is up-to-date; skipping"
          fi
        else
          # .po file does not exist -> copy generated file
          copy_po $_fname $lang
          echo "locale/$lang/LC_MESSAGES/$_fname.po generated"
          NEW_OR_UPDATED=1
          # remove now outdated .mo file
          remove_mo $_fname $lang
        fi
      done
      rm $_fname.pot
    done
    if [ $NEW_OR_UPDATED -eq 1 ]; then
      echo -e "$(tput bold)Some .po files were updated.\nUse \`./translate.sh compile\` after filling out the .po files to recompile them."
    fi
    ;;

  "regenerate")
    # regenerates the .po files for $2
    # removes all .mo files for $2
    _filename="$(basename $2)"
    _fname=${_filename%.*}
    echo "processing $_fname"
    xgettext -d $_fname -o $_fname.pot $2
    for lang in ${LOCALES[@]}
    do
      copy_po $_fname $lang
      remove_mo $_fname $lang
    done
    rm $_fname.pot
    echo -e "$(tput bold)All .po files for $_fname were regenerated.\nUse \`./translate.sh compile\` after filling out the .po files to recompile them."
    ;;

  "update")
    # updates the .po files for $2
    # removes all .mo files for $2
    _filename="$(basename $2)"
    _fname=${_filename%.*}
    echo "processing $_fname"
    xgettext -d $_fname -o $_fname.pot $2
    for lang in ${LOCALES[@]}
    do
      update_po $_fname $lang
      remove_mo $_fname $lang
    done;
    rm $_fname.pot
    echo -e "$(tput bold)All .po files for $_fname were updated.\nUse \`./translate.sh compile\` after filling out the .po files to recompile them."
    ;;

  "compile")
    # compiles .mo files for all files in $ALL_FILES, if the corresponding .po file exists
    for file in ${ALL_FILES[@]}
    do
      _filename="$(basename $file)"
      _fname=${_filename%.*}
      echo "processing $_fname"
      for lang in ${LOCALES[@]}
      do
        if [ -f $LOCALEDIR/$lang/LC_MESSAGES/$_fname.po ]; then
          msgfmt -o $LOCALEDIR/$lang/LC_MESSAGES/$_fname.mo $LOCALEDIR/$lang/LC_MESSAGES/$_fname.po
        else
          echo "$LOCALEDIR/$lang/LC_MESSAGES/$_fname.po does not exist; skipping"
        fi
      done
    done
    echo "$(tput bold)Compilation completed."
    ;;

  "cleanup")
    rm -rf $LOCALEDIR
    echo "$(tput bold)Removed $LOCALEDIR"
    ;;

  *)
    echo -e "USAGE:\n\t./translate.sh (autogen|compile|cleanup)\n\t./translate.sh (regenerate|update) file"
    exit 1
    ;;
esac

