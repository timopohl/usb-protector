# Deprecation notice

This repository is no longer maintained.
Development of a project with similar goals happens at https://gitlab.com/pohlarized/usb-protector (which may or may not be public at the time of reading this).

# What is this
The USB Protector project aims to provide a best effort USB protection against currently known
attacks, while still being usable for everyone without special technical knowledge, and without
introducing too much complexity compared to using USB without any additional controlling mechanism.

Therefore, it is **NOT** intended to make your computer completely safe against any USB attack, or
replace tools like [usbauth](https://github.com/kochstefan/usbauth-all), [USBGuard](https://usbguard.github.io/)
or [ukip](https://github.com/google/ukip). Rather, it is supposed to be an easy to use, basically
configurationless alternative, protecting the average user against the average USB attack.

The current implementation is built to work with the GNOME desktop environment, and has only been
tested there.

Currently, the notifications and Gtk app are available in german and english.

# Disclaimer
This is a proof of concept implementation created for usability research.
The tool has undergone very little practical testing, and should therefore be considered
experimental software. Bugs can and will likely happen. As this is interacting with the availability
of USB devices, a bug may very well stop all your USB devices from working. **If you want to use the
software at this time, make sure that you know how to recover your system from potentially booting
without USB support.** E.g. know how to disable the systemd service from a live system, or have PS/2
or other non-usb input devices ready to recover your system.

# Dependencies

## manjaro 20.0.3

- gobject-introspection
- gobject-introspection-runtime
- pkgconf
- gcc
- base-devel
- python-virtualenv

```
sudo pacman -Sy gobject-introspection gobject-introspection-runtime pkgconf gcc base-devel python-virtualenv
```
## debian 10 / ubuntu 20.04

- libgirepository1.0-dev
- libdbus-glib-1-dev
- gcc
- libcairo2-dev
- pkg-config
- python3-dev
- python3-cairo-dev
- python3-pip
- python3-virtualenv
- gir1.2-gtk-3.0

```
sudo apt install libgirepository1.0-dev libdbus-glib-1-dev gcc libcairo2-dev pkg-config python3-dev python3-cairo-dev python3-pip python3-virtualenv gir1.2-gtk-3.0
```

## fedora 32

- gcc
- gobject-introspection-devel
- cairo-devel
- cairo-gobject-devel
- pkg-config
- python3-devel
- python3-virtualenv
- dbus-python-devel
- gtk3

```
sudo dnf install gcc gobject-introspection-devel cairo-devel cairo-gobject-devel pkg-config python3-devel python3-virtualenv dbus-python-devel gtk3
```

# Installation

Simply execute the `install.sh` script to install the USB Protector. You can uninstall it with the
`uninstall.sh` script, which will also clean up any data that the USB Protector left on your system.
The installation has been tested on Fedora 32, Debian 10, Ubuntu 20.04 and Manjaro 20.0.3, but
should work on any other distribution with the gnome-shell installed.

```bash
cd /tmp
wget https://gitlab.gnome.org/timopohl/usb-protector/-/archive/master/usb-protector-master.zip
unzip usb-protector-master.zip
cd usb-protector-master
./install.sh
```

# Usage

## Notifications
After the installation, all the services automatically start, and they also start on every boot /
login. When a new device is connected, you get a notification like this:

![Mouse notification](img/notif_mouse.jpg)
![Keyboard notification](img/notif_keyboard.jpg)

The notifications allow you to block or trust a device. A blocked device will not work, while a
trusted device will have access to all drivers it asks the system for (so just like if no protection
is enabled). Trusted devices will also automatically work after a system reboot (or a service
restart), while blocked devices will stay blocked.

Additionally, the daemon sets devices that want to use keyboard functionality into a
"Keyboard-mode", which means that only keyboard functionality works for the device, and only keys
that are essential to authenticate yourself with a password are allowed. If that device sends a
different key to the computer while in Keyboard-mode, it will get blocked.

![Keyboard blocked](img/block_keyboard.jpg)

There is also a "Keyboard-Mouse-Mode", which will get active for devices that want both, keyboard
and mouse functionality. The device first goes into Keyboard-Mode, and if no additional action is
taken by the user, after 10 seconds it will go into Keyboard-Mouse-Mode. Then both, the keyboard and
mouse functionality will be available.

## GUI
There's also a GUI application that you can either open through the "Settings" button on a
notification, or via the application menu. In there, you can trust or block devices after the
notifications are gone.

Additionally, you can add or remove a device to/from a whitelist.
All devices with the same product ID and vendor ID (roughly translates to product name and vendor
name) as a whitelisted device will always automatically be trusted.

At last, it allows you to set the strictness mode. There are three strictness modes:
- **Silent**: You only get notifications when a device has automatically been blocked or set to
  Keyboard-[Mouse-]Mode by the daemon. Most new devices get trusted automatically.
- **Notify** (default): You get a notification for every newly connected device. Most new devices
  get trusted automatically.
- **Paranoid**: You get a notification for every newly connected device. No new device gets trusted
  automatically.

![GUI](img/gui.jpg)

# Recommendations

Use the `Panel OSD` extension from [here](https://extensions.gnome.org/extension/708/panel-osd/)
for a more pleasant notification experience. In the extension settings, enable "Force showing
details for all notifications" for now. SoonTM the stock notifications should be better since
there is currently a [GSoC project](https://summerofcode.withgoogle.com/projects/#5288799459868672)
happening regarding the notifications.

Also, the `Expandable Notifications` extension from
[here](https://extensions.gnome.org/extension/4463/expandable-notifications/) might prove to be
useful, as it allows you to expand notifications with buttons inside your notification history. The
`Auto` mode that automatically expands all notifications would probably be most useful in
combination with the usb-protector.

Ubuntu 20.04 does not support extensions by default. You can refer to [this askubuntu
answer](https://askubuntu.com/a/1107849) to enable them.

After the installation, go to https://extensions.gnome.org/local/ and click on the blue settings
icon for the Panel OSD extension. In the pop-up dialog, you can enable the "Force showing details
for all notifications" setting.

# How it works
Below you see a flowgraph of what is intended to happen with a device when it gets plugged in.
However, the process does only follow the principle of this graph, and it's not actually the flow
(e.g. it is not actually checked whether we are in the boot phase of the daemon every time a new
device is plugged in).

Budget Legend:
- Red and bold: Device gets blocked
- Orange and solid: Some select interfaces get authorized (text says which)
- Green and dashed: Device gets authorized
![Flow Graph](img/concept.jpg)
