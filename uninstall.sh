#!/bin/sh
# installs the usb_protector as a systemctl service

VENV_PATH='/etc/usb_protector/.usb_protector_venv'
DAEMON_BIN_PATH='/usr/sbin/usb_protector_daemon'
GUI_BIN_PATH='/usr/bin/usb_protector_gui'
NOTIF_BIN_PATH='/usr/bin/usb_protector_notification_daemon'
DATA_PATH='/etc/usb_protector/'
DAEMON_SYSTEMD_PATH='/usr/lib/systemd/system/usb_protector.service'
NOTIF_SYSTEMD_PATH='/usr/lib/systemd/user/usb_protector_notification_daemon.service'

# stop and disable systemd services
sudo systemctl stop usb_protector.service
sleep 1
sudo systemctl disable usb_protector.service

# remove service
sudo rm -f $DAEMON_SYSTEMD_PATH

# remove notification daemon
sudo rm -f /etc/xdg/autostart/usb_protector_notification_daemon.desktop

# remove executables
sudo rm -f $DAEMON_BIN_PATH
sudo rm -f $GUI_BIN_PATH
sudo rm -f $NOTIF_BIN_PATH

# remove venv
rm -rf $VENV_PATH

# remove data
sudo rm -rf $DATA_PATH

# remove gui desktop entry
sudo rm -f '/usr/share/applications/usb_protector_gui.desktop'

