#!/usr/bin/env python3

from __future__ import annotations
import asyncio
import enum
import os
import logging
import time
import sys
import subprocess
import signal
import traceback
from os import path
from threading import Thread, Lock
from typing import Any, Dict, List, Set, Union, Optional

import usb.core
import pyudev
import evdev
import notify2
import yaml
import zmq
from pydbus import SessionBus


SETTINGS_PATH = '/etc/usb_protector/'
WHITELIST_PATH = path.join(SETTINGS_PATH, 'whitelist.yml')
KNOWN_DEVICE_PATH = path.join(SETTINGS_PATH, 'known_devices.yml')
STRICTNESS_LEVEL_PATH = path.join(SETTINGS_PATH, 'strictness_level.yml')
KEYBOARD_MODE_PATH = path.join(SETTINGS_PATH, 'keyboard_mode.yml')
ROOT_LOGGER = logging.getLogger()
LOGLEVEL = logging.DEBUG
UID_LOCK = Lock()
with open(path.join(SETTINGS_PATH, 'zmq_config.yml'), 'r') as _stream:
    ZMQ_CONFIG: Dict[str, Union[int, str]] = yaml.safe_load(_stream)


class DeviceStatus(enum.Enum):
    NOOP = -2
    DISCONNECTED = -1
    WHITELISTED = 0
    TRUSTED = 1
    KEYBOARD_MODE = 2
    KEYBOARD_MOUSE_MODE = 3
    BLOCKED = 4


class StrictnessLevel(enum.Enum):
    LOW = 0
    MEDIUM = 1
    HIGH = 2


class UIDLock():
    """
    A mutex context manager to ensure that certain code passages are executed with a given euid
    Some actions (e.g. checking for the lockscreen status of a user) require to be executed with
    the euid of the user, while other actions need to be executed with the euid of root (0).

    Attributes
    ----------
    uid : int
        The UID that you want to use as the processes euid while inside the context.
    """
    def __init__(self, uid: int) -> None:
        self.uid = uid

    def __enter__(self) -> None:
        UID_LOCK.acquire()
        os.seteuid(self.uid)

    def __exit__(self, *args: Any) -> None:
        os.seteuid(0)
        UID_LOCK.release()


# TODO: implement filelocks (py-filelock) for files that the GUI could also write to


class KeyMonitor:
    """
    Creates threads that monitor the keys of a given USB Device, and deauthorizes the device when
    they hit a non allowed key.

    Attributes
    ----------
    logger : logging.Logger
        The logger for the KeyMonitor.
    allowed_scancodes : List[int]
        A list of scancodes that device in keyboard mode are allowed to press without getting
        deauthorized.
    """
    logger = logging.getLogger('KeyMonitor')
    #                   ESC + number row      qwe - []ENTER        a - ` and LSHIFT
    allowed_scancodes = (list(range(1, 15)) + list(range(15, 29)) + list(range(30, 43)) +
                         # \zx - / and RSHIFT  space  CAPSLOCK NUMLOCK Keypad
                         list(range(43, 55)) + [57] + [58] + [69] + list(range(71 - 83)) +
                         # Keypad extra     Arrows + Home + End              RALT    DEL
                         [55, 83, 96, 98] + [102, 103, 105, 106, 107, 108] + [100] + [111])

    @classmethod
    def _get_input_devices(cls, device: USBDevice) -> List[pyudev.Device]:
        """Get the corresponding input devices (from /dev/input) to a USBDevice (which matches
        through their mapping in the sysfs).

        Parameters
        ----------
        device : USBDevice
            The device of which you want to get the input data.

        Returns
        -------
        Optional[pyudev.Device]
            The found device, or None if no matching input device was found.

        """
        tries = 5
        input_devices: List[pyudev.Device] = []
        context = pyudev.Context()
        # the creation of input devices can take a while, so we might look for an input device too
        # early before it has been creates
        while tries > 0 and not input_devices:
            udev_dev: pyudev.Device
            for udev_dev in context.list_devices(subsystem='input'):
                if device.udev_dev.sys_path not in udev_dev.sys_path:
                    # device sys paths don't match up
                    continue
                if not udev_dev.device_node:
                    # device is not fully initialized
                    continue
                if udev_dev in input_devices:
                    # device is already in the list of devices to monitor
                    continue
                input_devices.append(udev_dev)
            time.sleep(0.1)
            tries -= 1
        return input_devices

    @classmethod
    def _monitor_device(cls, device: USBDevice, evdev_device: evdev.InputDevice) -> None:
        """Executed in a thread! Monitors an input device with evdev and deauthorizes it when it is
        hitting a key that its not authorized to hit.

        Parameters
        ----------
        device : USBDevice
            The device that you want to limit the keys of.
        evdev_device : evdev.InputDevice
            The evdev interface to the device that you want to limit the keys of.

        Returns
        -------
        None

        """
        asyncio.set_event_loop(asyncio.new_event_loop())
        with UIDLock(0):
            with open(KEYBOARD_MODE_PATH, 'r') as stream:
                keyboard_mode = yaml.safe_load(stream)
            if device.identifier not in keyboard_mode:
                keyboard_mode.append(device.identifier)
                with open(KEYBOARD_MODE_PATH, 'w') as stream:
                    yaml.safe_dump(keyboard_mode, stream)
        try:
            for event in evdev_device.read_loop():
                if event.type != evdev.ecodes.EV_KEY:
                    continue
                with open(KEYBOARD_MODE_PATH, 'r') as stream:
                    keyboard_mode = yaml.safe_load(stream)
                if device.identifier not in keyboard_mode:
                    # stop monitoring the device
                    cls.logger.info('Keys for device %s have manually been unlimited, stopping '
                                    'the monitoring thread', device.identifier)
                    break
                scancode = evdev.categorize(event).scancode
                if scancode not in cls.allowed_scancodes:
                    # deauthorize before log to be as quick as possible, and not allow the device to
                    # send more keys
                    device.deauthorize_interfaces()
                    cls.logger.warning('Device %s has triggered a forbidden key, deauthorizing!',
                                       device.identifier)
                    remove_from_keyboard_mode(device.identifier)
                    remove_from_known_devices(device.identifier)
                    TimeoutManager.remove_device_path(device.udev_dev.sys_path)
                    status = {
                        'sys_path': device.udev_dev.sys_path,
                        'identifier': device.identifier,
                        'capabilities': device.capabilities,
                        'status': DeviceStatus.BLOCKED.value
                    }
                    IPCClient.update_status(status)
                    IPCClient.prompt_notification('keyboard_block',
                                                  device,
                                                  scancode)
                    return
        except OSError as exc:
            if exc.errno == 19:
                cls.logger.warning('Device %s disconnected, stopping monitoring', device.identifier)
            else:
                raise

    @classmethod
    def limit_keys(cls, device: USBDevice) -> None:
        """Get all input devices for a USB device, and then start monitoring threads for them.

        Parameters
        ----------
        device : USBDevice
            The device that you want to monitor.

        Returns
        -------
        None

        """
        input_devices = cls._get_input_devices(device)
        if not input_devices:
            cls.logger.critical('Could not get input device for device %s',
                                device.udev_dev.sys_path)
            return
        for input_device in input_devices:
            try:
                with UIDLock(0):
                    evdev_device = evdev.InputDevice(input_device.device_node)
            except OSError as exception:
                cls.logger.critical('Could not get evdev dev for device %s OSError: %s',
                                    device.udev_dev.sys_path,
                                    exception)
                continue
            cls.logger.info('Monitoring keys for device %s with node %s',
                            device.identifier,
                            device.udev_dev.device_node)
            thread = Thread(target=cls._monitor_device, args=(device, evdev_device), daemon=True)
            thread.start()


class TimeoutManager:
    """Provides methods to allow setting a timeout after which a certain set of interfaces of a
    device will be authorized.

    Attributes
    ----------
    logger : logging.Logger
        The logger for the TimeoutManager.
    device_lock : threading.Lock
        A Mutex making sure the timeout_device_paths stay in sync.
    timeout_device_paths : List[str]
        A list of all syspaths of the devices for which the timeout loop should run.
    """
    logger = logging.getLogger('TimeoutManager')
    device_lock = Lock()
    timeout_device_paths = []  # list of syspaths of devices for which the timeout loop should run
                               # used to stop timeout loops of certain devices from the outside

    @classmethod
    def remove_device_path(cls, path: str) -> bool:
        """Stop the timer for the device corresponding to path. This does *not* lead to the device
        being authorized.

        Parameters
        ----------
        path : str
            The path do the device that you no longer want to get allowed after the timeout.

        Returns
        -------
        bool
            Whether or not the device_path was in the timeout devices in the first place.

        """
        cls.logger.debug('Removing device @%s from timeout devices', path)
        ret = False
        with cls.device_lock:
            if path in cls.timeout_device_paths:
                cls.timeout_device_paths.remove(path)
                ret = True
        return ret

    @classmethod
    def timed_allow(cls, device: USBDevice, interfaces: List[USBInterface], timeout: int=10) -> Thread:
        """Allow a set of tinterfaces for a USBDevice to be authorized after a given timeout.

        Parameters
        ----------
        device : USBDevice
            The device of which you want to allow certain interfaces.
        interfaces : List[USBInterface]
            The interfaces of the device that you want to allow.
        timeout : int
            The timeout in seconds after which you want to allow the specified interfaces.

        Returns
        -------
        Thread
            The thread which performs the timout task.

        """
        path = device.udev_dev.sys_path
        cls.logger.debug('Starting timed allow for device @%s', path)
        cls._add_device_path(path)
        thread = Thread(target=cls._timed_allow, args=(device, interfaces, timeout), daemon=True)
        thread.start()
        return Thread

    @classmethod
    def _add_device_path(cls, path: str) -> bool:
        """Add the syspath of a device to the list of devices to keep the timout loop running for.

        Parameters
        ----------
        path : str
            Syspath of the device of which you want to keep the timout loop running.

        Returns
        -------
        bool
            True if the device has not already been in the list, False otherwise.

        """
        cls.logger.debug('Adding device @%s to timeout devices', path)
        ret = False
        with cls.device_lock:
            if path not in cls.timeout_device_paths:
                cls.timeout_device_paths.append(path)
                ret = True
        return ret

    @classmethod
    def _timed_allow(cls, device: USBDevice, interfaces: List[USBInterface], timeout: int) -> None:
        """Will authorize the interfaces after <timeout> seconds, unless a manual action has been
        made through the GUI or a notification action, or the attached keyboard got blocked.

        Parameters
        ----------
        device : USBDevice
            Thee device which you want to allow some interfaces of.
        interfaces : List[USBInterface]
            The interfaces which you want to authorize after the timeout.
        timeout : int
            The timeout in seconds after which you want to authorize the specified interfaces.

        Returns
        -------
        None

        """
        path = device.udev_dev.sys_path
        time.sleep(timeout)
        allow = False
        with cls.device_lock:
            if path in cls.timeout_device_paths:
                cls.timeout_device_paths.remove(path)
                allow = True
        if not allow:
            cls.logger.debug('Device @%s has already been manually handled, not authing', path)
            return
        cls.logger.debug('Device @%s has timed out, allowing specified interfaces!', path)
        for interface in interfaces:
            interface.authorize()
        for interface in interfaces:
            interface.probe()
        if device.is_full_authorized():
            add_to_known_devices(device.identifier)

        # no manual change has been made -> was in kb mode, now in kbm mode
        device_info = {
            'name': device.name,
            'identifier': device.identifier,
            'sys_path': device.udev_dev.sys_path,
            'capabilities': device.capabilities,
            'status': DeviceStatus.KEYBOARD_MOUSE_MODE.value
        }
        IPCClient.update_status(device_info)

class USBDevice:
    """Representation of a single physical USB device.

    Attributes
    ----------
    logger : logging.Logger
        The logger for the USB device.
    udev_dev : pyudev.Device
        A udev device represending the USB device.
    pid : str
        The value of the USB devices idProduct field.
    vid : str
        The value of the USB devices idVendor field.
    dev_cls : str
        The value of the USB devices bDeviceClass field.
    dev_subcls : str
        The value of the USB devices bDeviceSubClass field.
    dev_proto : str
        The value of the USB devices bDeviceProtocol field.
    usblib_dev : usb.core.Device
        A usblib device representing the USB device.
    identifier : str
        An identifier used by the known devices list, in the form
        "idVendor:idProduct:active_cfg_no".
    name : str
        The name of the device in the form "vendor_name product_name"
    capabilities : Set[str]
        Capability IDs of all interfaces that the device has. Capability IDs consist of an
        interfaces class, subclass and protocol.
    """
    def __init__(self, device: pyudev.Device) -> None:
        """Constructor method.

        Parameters
        ----------
        device : pyudev.Device
            A udev device representing the USB device.

        Returns
        -------
        None

        """
        self.logger = logging.getLogger('USBDevice')
        self.udev_dev = device
        attrs: pyudev.Attributes = device.attributes
        self.pid = attrs.asstring('idProduct').strip()
        self.vid = attrs.asstring('idVendor').strip()
        self.dev_cls = attrs.asstring('bDeviceClass').strip()
        self.dev_subcls = attrs.asstring('bDeviceSubClass').strip()
        self.dev_proto = attrs.asstring('bDeviceProtocol').strip()
        self.usblib_dev: usb.core.Device = usb.core.find(idProduct=int(self.pid, 16),
                                                         idVendor=int(self.vid, 16))
        self.identifier = self._build_identifier()
        self.name = self._get_name()
        self.capabilities = self._get_capability_ids()

    @classmethod
    def from_path(cls, sys_path: str) -> USBDevice:
        """Instantiates a USBDevice from a given sys_path

        Parameters
        ----------
        sys_path : str
            The path in the sysfs to the deviec

        Returns
        -------
        USBDevice
            A USBDevice object corresponding to the device with the given syspath

        """
        context = pyudev.Context()
        for device in context.list_devices(subsystem='usb'):
            if device.device_type != 'usb_device':
                continue
            if device.sys_path == sys_path:
                return USBDevice(device)
        raise RuntimeError(f'Could not find device with path {sys_path}')

    def _get_capability_ids(self) -> Set[str]:
        """Get the USB Interface class, subclass and protocol values of all interfaces of the device

        Parameters
        ----------

        Returns
        -------
        Set[str]
            The capability IDs that each are in the form "class-subclass:protocol", one for each
            interface of the device.

        """
        capability_ids = set()
        for interface in self._get_real_interfaces():
            if interface.if_cls == '00':
                if self.dev_cls == '00':
                    if_cls = 'ff'
                else:
                    if_cls = self.dev_cls
            else:
                if_cls = interface.if_cls
            capability_id = f'{if_cls}-{interface.if_subcls}:{interface.if_proto}'
            capability_ids.add(capability_id)
        return capability_ids

    def _get_name(self) -> str:
        """Get the vendor and product name that the device provides.

        Parameters
        ----------

        Returns
        -------
        str
            The name in the form "vendor product".

        """
        completed = subprocess.run(['lsusb'], capture_output=True, check=True)
        usb_devices = completed.stdout.decode('utf-8')
        lines = usb_devices.split('\n')
        for line in lines:
            if line == '':
                continue
            attrs = line.split(' ', 6)
            name = attrs[-1]
            identifier = attrs[-2]
            if identifier == f'{self.vid}:{self.pid}':
                return name
        vendor = usb.util.get_string(self.usblib_dev, self.usblib_dev.iManufacturer)
        product = usb.util.get_string(self.usblib_dev, self.usblib_dev.iProduct)
        return f'{vendor} {product}'

    def _build_identifier(self) -> str:
        """Build the identifier used to reindentify a device across multiple plug-ins / reboots.

        The identifier uses all fields of the device that determine the drivers that are getting
        bound to it, so even if a device is misidentified it can only bind the same drivers as the
        devices that it was misidentified as.

        Parameters
        ----------

        Returns
        -------
        str
            An identifier in the form
            "vid:pid:dev_cls:dev_subcls:dev_proto:bcd_device:interfaces_identifier", where the
            interfaces_identifier is a string in the form "if_cls:if_subcls_if_proto" concatenated
            for all interfaces of the device.

        """
        attrs: pyudev.Attributes = self.udev_dev.attributes
        bcd_device = attrs.asstring('bcdDevice').strip()
        interfaces = self._get_real_interfaces()
        interface_ids = set()
        for interface in interfaces:
            interface_ids.add(f'{interface.if_cls}:{interface.if_subcls}:{interface.if_proto}')
        interfaces_identifier = ':'.join(sorted(interface_ids))
        return (f'{self.vid}:{self.pid}:{self.dev_cls}:{self.dev_subcls}:{self.dev_proto}:'
                f'{bcd_device}:{interfaces_identifier}')

    def _get_real_interfaces(self) -> List[USBInterface]:
        """Get a list of the immediat (non-transitive, e.g. in case of a hub) interfaces of the
        device.

        Parameters
        ----------

        Returns
        -------
        List[USBInterface]
            All immediate interfaces of the device.

        """
        children = []
        child_if: pyudev.Device
        for child_if in self.udev_dev.children:
            if child_if.device_type != 'usb_interface':
                continue
            sub_path = child_if.sys_path[len(self.udev_dev.sys_path) + 1:]
            if '/' in sub_path:
                # only a transitive interface (e.g. of a subdevice of a hub)
                continue
            usbif = USBInterface(child_if, self)
            children.append(usbif)
        return children

    def authorize(self) -> bool:
        """Authorize the device.

        Parameters
        ----------

        Returns
        -------
        bool
            Indication whether the authorization was successful or not

        """
        with UIDLock(0):
            try:
                with open(path.join(self.udev_dev.sys_path, 'authorized'), 'w') as stream:
                    stream.write('1')
            except FileNotFoundError:
                return False
        return True


    def deauthorize(self) -> None:
        """Deauthorize the device.

        Parameters
        ----------

        Returns
        -------
        None

        """
        with UIDLock(0):
            try:
                with open(path.join(self.udev_dev.sys_path, 'authorized'), 'w') as stream:
                    stream.write('0')
            except FileNotFoundError:
                self.logger.warning('Tried to deauthorize device %s that is not present anymore',
                                    self.identifier)
        remove_from_known_devices(self.identifier)
        self.logger.info('Deauthorized device %s', self.identifier)

    def is_authorized(self) -> bool:
        """Check whether the device itself is authorized.

        Parameters
        ----------

        Returns
        -------
        bool

        """
        try:
            with open(path.join(self.udev_dev.sys_path, 'authorized'), 'r') as stream:
                status = stream.readline().strip()
        except FileNotFoundError:
            self.logger.warning('Tried to read authorization status of device %s which is not '
                                'present anymore', self.identifier)
            status = '0'
        return status == '1'

    def is_full_authorized(self) -> bool:
        """Check whether the device itself, and all of its interfaces are authorized.

        Parameters
        ----------

        Returns
        -------
        bool
            True, if the deivce AND all of its interfaces are authorized, else False.

        """
        if not self.is_authorized:
            return False
        for child_if in self._get_real_interfaces():
            if not child_if.is_authorized():
                return False
        return True

    def get_auth_state(self) -> int:
        """Infer the current authorization state of the device.

        Parameters
        ----------

        Returns
        -------
        int
            The auth state value, corresponding to the DeviceStatus Enum.

        """
        if self.identifier in get_whitelist().keys():
            return DeviceStatus.WHITELISTED.value
        with open(KEYBOARD_MODE_PATH, 'r') as stream:
            keyboard_mode_devices = yaml.safe_load(stream)
        if self.identifier in keyboard_mode_devices:
            return DeviceStatus.KEYBOARD_MODE.value
        if self.is_full_authorized():
            return DeviceStatus.TRUSTED.value
        return DeviceStatus.BLOCKED.value

    def authorize_interfaces(self) -> bool:
        """Authorize all interfaces of the device but do NOT authorize sub devices (e.g. of a HUB).

        Parameters
        ----------

        Returns
        -------
        bool
            Indication whether authorization and probing of *all* interfaces was successful
        """
        interfaces_to_probe: List[USBInterface] = []
        success = True

        config: usb.core.Configuration = self.usblib_dev.get_active_configuration()
        pyusb_if: usb.core.Interface
        for pyusb_if in config.interfaces():
            pyudev_if = self.usbinterface_from_pyusb_interface(pyusb_if)
            success &= pyudev_if.authorize()
            interfaces_to_probe.append(pyudev_if)
        for pyudev_if in interfaces_to_probe:
            success &= pyudev_if.probe()
        return success

    def deauthorize_interfaces(self) -> None:
        """Deauthorize all interfaces of the device.

        Parameters
        ----------

        Returns
        -------
        None

        """
        config: usb.core.Configuration = self.usblib_dev.get_active_configuration()
        pyusb_if: usb.core.Interface
        for pyusb_if in config.interfaces():
            pyudev_if = self.usbinterface_from_pyusb_interface(pyusb_if)
            pyudev_if.deauthorize()

    def init(self) -> None:
        """Called on all devices on first start of the daemon. Goes through the
        initialization step of a USB Device.

        If the device is whitelisted, allow all interfaces.
        Otherwise, allow all interfaces if the device is on the list of known
        devices. If not, only allow (and limit) the keyboard interface.

        Parameters
        ----------

        Returns
        -------
        None

        """
        def init_children() -> None:
            for udev_child in self.udev_dev.children:
                if udev_child.device_type != 'usb_device':
                    continue
                child_device = USBDevice(udev_child)
                self.logger.debug('Looking at child device %s of device %s',
                                  child_device.identifier,
                                  self.identifier)
                child_device.init()

        self.logger.debug('Initializing device %s', self.identifier)
        # authorize all interfaces of devices on the whitelist
        if self.identifier in get_whitelist().keys():
            self.logger.debug('Device %s matched whitelist, authorizing', self.identifier)
            self.authorize_interfaces()
            init_children()
            return

        if self.identifier in get_known_devices():
            self.logger.debug('Device %s is a known device, authorizing', self.identifier)
            self.authorize_interfaces()
            init_children()
            return

        # iterate over children once to allow all interfaces that are to be allowed
        # if its a hub, a new device might have appeared in the children (but not a
        # new interface), so iterate over the children again but only look at
        # devices
        config: usb.core.Configuration = self.usblib_dev.get_active_configuration()
        pyusb_if: usb.core.Interface
        for pyusb_if in config.interfaces():
            interface = self.usbinterface_from_pyusb_interface(pyusb_if)
            self.logger.debug('Looking at interface %s of device %s',
                              interface.identifier,
                              self.identifier)
            if interface.if_cls == '03' and interface.if_proto == '01':
                # interface is a keyboard
                self.logger.warning('Unknown device %s has a keyboard interface %s, authorizing '
                                    'this interface and limiting its keys...',
                                    self.identifier, interface.identifier)
                interface.authorize()
                interface.probe()  # we can probe right away because usbhid only needs one interface
                KeyMonitor.limit_keys(self)
        init_children()
        self.logger.debug('Device %s initialized', self.identifier)

    def usbinterface_from_pyusb_interface(self,
                                          pyusb_interface: usb.core.Interface) -> USBInterface:
        """Given a usb.core.Interface object that belongs to this device, create a USBInterface
        object that represents the same interface.

        Parameters
        ----------
        pyusb_interface : usb.core.Interface
            pyusb_interface

        Returns
        -------
        USBInterface
            The USBInterface representation of the USB interface

        """
        if_cls = hex(pyusb_interface.bInterfaceClass)[2:]
        if_no = hex(pyusb_interface.bInterfaceNumber)[2:]
        # prepend zeroes to fit the 1 byte string representation
        # TODO: surely there is a more beautiful method to fix this.
        if len(if_cls) < 2:
            if_cls = f'0{if_cls}'
        if len(if_no) < 2:
            if_no = f'0{if_no}'
        identifier = f'{if_no}:{if_cls}'
        child_udev_dev: pyudev.Device
        for child_udev_dev in self.udev_dev.children:
            if child_udev_dev.device_type != 'usb_interface':
                continue
            interface = USBInterface(child_udev_dev, self)
            if interface.identifier == identifier:
                return interface
        raise ValueError('Could not find a matching udev interface for the device '
                         f'{self.identifier} given the identifier {identifier}')


class USBInterface:
    """Class representing a USB interface.

    Attributes
    ----------
    logger : logging.Logger
        The logger for the USB interface.
    udev_dev : pyudev.Device
        A udev device representing the USB interface.
    parent : USBDevice
        A USBDevice representing the USB device that the interface belongs to.
    if_no : str
        The value of the bInterfaceNumber field.
    if_cls : str
        The value of the bInterfaceClass field.
    if_proto : str
        The value of the bInterfaceProtocol field.
    identifier : str
        An identifier in the form of "bInterfaceNumber:bInterfaceClass"
    """

    def __init__(self, device: pyudev.Device, parent: USBDevice) -> None:
        self.logger = logging.getLogger('USBInterface')
        self.udev_dev = device
        self.parent = parent
        attrs: pyudev.Attributes = device.attributes
        self.if_no = attrs.asstring('bInterfaceNumber').strip()
        self.if_cls = attrs.asstring('bInterfaceClass').strip()
        self.if_subcls = attrs.asstring('bInterfaceSubClass').strip()
        self.if_proto = attrs.asstring('bInterfaceProtocol').strip()
        self.identifier = f'{self.if_no}:{self.if_cls}'

    def authorize(self) -> bool:
        """Authorize the interface.

        Parameters
        ----------

        Returns
        -------
        bool
            indicates whether authorization was successful

        """
        sys_path = self.udev_dev.sys_path
        with UIDLock(0):
            try:
                with open(path.join(sys_path, 'authorized'), 'w') as stream:
                    stream.write('1')
            except FileNotFoundError:
                self.logger.warning('Tried to authorize interface %s at %s, which is not present '
                                    'anymore', self.identifier, self.udev_dev.sys_path)
                return False
        self.logger.debug('Authorized interface %s at %s', self.identifier, self.udev_dev.sys_path)
        return True

    def deauthorize(self) -> None:
        """Deauthorize the interface.

        Parameters
        ----------

        Returns
        -------
        None

        """
        with UIDLock(0):
            try:
                with open(path.join(self.udev_dev.sys_path, 'authorized'), 'w') as stream:
                    stream.write('0')
            except FileNotFoundError:
                self.logger.warning('Tried to deauthorize interface %s at %s, which is not present '
                                    'anymore', self.identifier, self.udev_dev.sys_path)
                return
        self.logger.debug('Deauthorized interface %s at %s',
                          self.identifier,
                          self.udev_dev.sys_path)

    def probe(self) -> bool:
        """Probe the driver for the interface (essentially tells the kernel to bind a driver to it)

        Parameters
        ----------

        Returns
        -------
        bool
            Indication whether probing the interface was successful
        """
        sys_path = self.udev_dev.sys_path
        if_name = sys_path.split('/')[-1]
        with UIDLock(0):
            try:
                with open('/sys/bus/usb/drivers_probe', 'w') as stream:
                    stream.write(if_name)
            except OSError:
                self.logger.warning('Tried to probe no longer existing interface %s at %s',
                                    self.identifier,
                                    self.udev_dev.sys_path)
                return False
        # FIXME this should wait for the interfaces to actually get a driver
        # before returning.
        if self.if_cls == '09':
            time.sleep(1.5)
        self.logger.debug('Probed driver for interface %s at %s',
                          self.identifier,
                          self.udev_dev.sys_path)
        return True

    def is_authorized(self) -> bool:
        """Tell whether or not the interface is authorized

        Parameters
        ----------

        Returns
        -------
        bool
            Indication whether the device is authorized or not

        """
        try:
            with open(path.join(self.udev_dev.sys_path, 'authorized'), 'r') as stream:
                status = stream.readline().strip()
        except FileNotFoundError:
            status = '0'
        return status == '1'


class IPCClient:
    """The IPC Client used to proactively broadcast device status updates or notification requests
    to the GUIs and Notification Daemons through sockets/zmq.

    Attributes
    ----------
    logger : logging.Logger
        The logger for the IPC client.
    context : zmq.Context
        The zmq context used to create sockets
    socket : zmq.Socket
        The socket that the IPC client sends the information to
    """

    logger = logging.getLogger('IPCClient')
    context = zmq.Context.instance()
    socket = context.socket(zmq.PUB)
    socket.bind(f'tcp://*:{ZMQ_CONFIG["SERVER_SEND_PORT"]}')

    @classmethod
    def update_status(cls, status: Dict[str, Any]) -> None:
        """Broadcast a status update of a device to all GUIs

        Parameters
        ----------
        status : Dict[str, Any]
            Contains device information about the deivce. For stati see the DeviceStatus values.

        Returns
        -------
        None

        """
        msg = yaml.safe_dump(status)
        msg = f'{ZMQ_CONFIG["GUI_PREFIX"]}{msg}'
        cls.logger.debug('Updating status: %s', repr(msg))
        cls.socket.send(msg.encode('utf-8'))

    @classmethod
    def prompt_notification(cls,
                            notification_type: str,
                            device: USBDevice,
                            hit_scancode: int = 0,
                            urgency: int = notify2.URGENCY_NORMAL) -> None:
        """Broadcast a request to show a notification to all NotificationDaemons

        Parameters
        ----------
        notification_type : str
            The type of notification. Types can be seen in the data/notifications_xx_XX.yml files.
        device : USBDevice
            The USBDevice that the notification is about.
        hit_scancode : int
            The scancode of th key that was pressed, if the notification type is keyboard_block.
        urgency : int
            The notification urgency. Changed based on strictness level. In paranoid (HIGH), urgency is set to CRITICAL.

        Returns
        -------
        None

        """
        strictness_level = get_strictness_level()
        if strictness_level is StrictnessLevel.HIGH:
            urgency = notify2.URGENCY_CRITICAL
        msg = yaml.safe_dump({'notification_type': notification_type,
                              'urgency': urgency,
                              'hit_scancode': hit_scancode,
                              'device': device.udev_dev.sys_path,
                              'device_name': device.name,
                              'device_capabilities': device.capabilities})
        msg = f'{ZMQ_CONFIG["NOTIFICATION_PREFIX"]}{msg}'
        cls.logger.debug('Prompting notification: %s', repr(msg))
        cls.socket.send(msg.encode('utf-8'))


class IPCServer:  # pylint:disable = too-few-public-methods
    """The IPC Server used to process requests from the GUIs and NotificationDaemons

    Attributes
    ----------
    logger : logging.Logger
        The logger for the IPC server
    pull_socket : zmq.Socket
        The socket that is used to receive requests
    rep_socket : zmq.Socket
        The socket that is used to receive requests and send back replies
    """

    def __init__(self) -> None:
        context = zmq.Context.instance()
        self.logger = logging.getLogger('IPCServer')
        self.pull_socket = context.socket(zmq.PULL)
        self.pull_socket.bind(f'tcp://*:{ZMQ_CONFIG["SERVER_RECV_PORT"]}')
        self.rep_socket = context.socket(zmq.REP)
        self.rep_socket.bind(f'tcp://*:{ZMQ_CONFIG["SERVER_REP_PORT"]}')

    def listen(self) -> None:
        """Starts the two server threads to listen on the different sockets.

        The pull_thread only receives requests and processes them (e.g. when a device status was
        updated through a GUI or Notification action), the rep_thread also replies to requests (e.g.
        when a GUI requests a list of the currently connected devices).


        Parameters
        ----------

        Returns
        -------
        None

        """
        pull_thread = Thread(target=self._listen_pull, daemon=True)
        pull_thread.start()
        rep_thread = Thread(target=self._listen_rep, daemon=True)
        rep_thread.start()

    def _listen_pull(self) -> None:
        """Listen for device updates from a GUI or notification.

        Parameters
        ----------

        Returns
        -------
        None

        """
        while True:
            self.logger.debug('Waiting for new messages...')
            msg = self.pull_socket.recv().decode('utf-8')
            self.logger.debug('Received msg: %s', repr(msg))
            if msg.startswith(ZMQ_CONFIG['GUI_PREFIX']):
                thread = Thread(target=self._handle_gui_request,
                                args=(msg[len(ZMQ_CONFIG['GUI_PREFIX']):],))
            elif msg.startswith(ZMQ_CONFIG['NOTIFICATION_PREFIX']):
                thread = Thread(target=self._handle_notification_request,
                                args=(msg[len(ZMQ_CONFIG['NOTIFICATION_PREFIX']):],))
            thread.start()

    def _listen_rep(self) -> None:
        """Listen for requests to list the connected USB devices.

        Parameters
        ----------

        Returns
        -------
        None

        """
        while True:
            self.logger.debug('Waiting for new requests...')
            msg = self.rep_socket.recv().decode('utf-8')
            self.logger.debug('Received request: %s', repr(msg))
            if msg == 'LIST_DEVICES':
                devices = self._list_devices()
                resp = yaml.safe_dump(devices)
                self.rep_socket.send(resp.encode('utf-8'))

    @staticmethod
    def _list_devices() -> List[Dict[str, str]]:
        """Create a list of all currently connected devices.

        Parameters
        ----------

        Returns
        -------
        List[Dict[str, str]]
            The list of all currently connected devices.

        """
        context = pyudev.Context()
        device_list = []

        udev_dev: pyudev.Device
        for udev_dev in context.list_devices(subsystem='usb'):
            if udev_dev.device_type != 'usb_device':
                continue
            if udev_dev.sys_path.split('/')[-1].startswith('usb'):
                # ignore root hubs
                continue
            usb_dev = USBDevice(udev_dev)
            info = {
                'sys_path': udev_dev.sys_path,
                'name': usb_dev.name,
                'identifier': usb_dev.identifier,
                'status': usb_dev.get_auth_state(),
                'capabilities': usb_dev.capabilities
            }
            device_list.append(info)
        return device_list

    def _handle_gui_request(self, msg: str) -> None:
        """Act upon an action a user took on a GUI.

        Parameters
        ----------
        msg : str
            A GUI request as YAML data

        Returns
        -------
        None

        """
        self.logger.debug('Received GUI request: %s', repr(msg))
        request = yaml.safe_load(msg)
        action = request['action']  # what did the user do
        if action == -1:  # This is a whitelist removal, NOT a device disconnection!
            identifier = request['identifier']
            remove_from_whitelist(identifier)
            return
        sys_path = request['sys_path']
        device = USBDevice.from_path(sys_path)
        # If the device status was changed, the device can no longer be in keyboard mode, nor on
        # timeout
        remove_from_keyboard_mode(device.identifier)
        TimeoutManager.remove_device_path(device.udev_dev.sys_path)

        dev_status = DeviceStatus(action)

        if dev_status is DeviceStatus.WHITELISTED:
            add_to_whitelist(device.identifier, device.name)
            if device.authorize_interfaces():
                add_to_known_devices(device.identifier)
        elif dev_status is DeviceStatus.TRUSTED:
            remove_from_whitelist(device.identifier)
            if device.authorize_interfaces():
                add_to_known_devices(device.identifier)
        elif dev_status is DeviceStatus.BLOCKED:
            remove_from_whitelist(device.identifier)
            remove_from_known_devices(device.identifier)
            device.deauthorize_interfaces()

    def _handle_notification_request(self, msg: str) -> None:
        """Act upon a notification action by a user.

        Parameters
        ----------
        msg : str
            A notification request as YAML data

        Returns
        -------
        None

        """
        self.logger.debug('Received notification request: %s', repr(msg))
        request = yaml.safe_load(msg)
        action = request['action']
        device = USBDevice.from_path(request['device'])
        TimeoutManager.remove_device_path(request['device'])

        device_info = {
            'name': device.name,
            'identifier': device.identifier,
            'sys_path': device.udev_dev.sys_path,
            'capabilities': device.capabilities,
            'status': DeviceStatus.NOOP.value
        }
        if action == 'auth':
            device_info['status'] = DeviceStatus.TRUSTED.value
            self.logger.debug('Trusting dev %s through notification action', device.identifier)
            remove_from_keyboard_mode(device.identifier)
            if device.authorize_interfaces():
                add_to_known_devices(device.identifier)
        elif action == 'deauth':
            device_info['status'] = DeviceStatus.BLOCKED.value
            self.logger.debug('Deauthorizing dev %s through notification action', device.identifier)
            remove_from_known_devices(device.identifier)
            device.deauthorize_interfaces()
        IPCClient.update_status(device_info)


# pylint: disable=too-few-public-methods
class USBProtectorDaemon:
    """The main background process monitoring USB device updates

    Takes action on device connects and disconnects, and decides on the initial state of new USB
    devices. Makes sure that Notifications and GUIs work.

    logger : logging.Logger
        The logger for the USB Protector daemon.
    sys_path_device_id_map : List[str, str]
        A mapping of sys paths to device ids, used to reidentify disconnected devices.
    """

    def __init__(self) -> None:
        self.logger = logging.getLogger('USBProtectorDaemon')
        # {sys_path: device_id}, used to map device disconnects back to device IDs
        self.sys_path_device_id_map: Dict[str, str] = {}

    def run(self) -> None:
        """Start the initialization and monitoring.

        Parameters
        ----------

        Returns
        -------
        None

        """
        if not path.isfile(KNOWN_DEVICE_PATH):
            # first time running the daemon
            self._init_known_devices()
        self._disable_auto_auth()
        self._lockdown()
        self._init_usb_system()
        # after the usb system has been initialized with all the allowed
        # devices, rewrite the known devices file with the currently
        # connected devices, to remove all devices that have been present at
        # the last service shutdown, but are not present anymore
        self._init_known_devices()
        try:
            self._monitor()
        except KeyboardInterrupt:
            # Triggered by systemctl restart/stop events
            print('Shutting down...')
            restore_automatic_auth()
            sys.exit()

    # initialization stage
    def _disable_auto_auth(self) -> None:
        """Stop new USB interfaces from being automatically authorized

        Parameters
        ----------

        Returns
        -------
        None

        """
        usb_bus: str
        for usb_bus in os.listdir('/sys/bus/usb/devices/'):
            if usb_bus.startswith('usb'):
                if_auth_file = path.join('/sys/bus/usb/devices/',
                                         usb_bus,
                                         'interface_authorized_default')
                with UIDLock(0):
                    with open(if_auth_file, 'w') as stream:
                        stream.write('0')
                self.logger.debug('Disabled automatic interface authorization for bus %s', usb_bus)

    def _lockdown(self) -> None:
        """Deauthorize all USB interfaces that are currently connected

        Parameters
        ----------

        Returns
        -------
        None

        """
        self.logger.debug('Starting usb lockdown')
        context = pyudev.Context()

        udev_device: pyudev.Device
        for udev_device in context.list_devices(subsystem='usb'):
            if udev_device.device_type != 'usb_device':
                continue
            parent = USBDevice(udev_device)
            for child_if in udev_device.children:
                if child_if.device_type != 'usb_interface':
                    continue
                interface = USBInterface(child_if, parent)
                interface.deauthorize()
        self.logger.info('USB lockdown done, all interfaces deauthorized')

    def _init_usb_system(self) -> None:
        """On first start of the saemon, authorize all known devices, and all keyboards in keyboard
        mode if they're not known.

        Parameters
        ----------

        Returns
        -------
        None

        """
        self.logger.info('Initializing USB system')
        context = pyudev.Context()

        udev_device: pyudev.Device
        # will only be the BUSes, because everything else needs their
        # interfaces activated
        for udev_device in context.list_devices(subsystem='usb'):
            if udev_device.device_type != 'usb_device':
                continue
            device = USBDevice(udev_device)
            device.init()
        self.logger.info('USB system initialized')

    # daemon stage
    def _monitor(self) -> None:
        """Watch for new USB devices

        Parameters
        ----------

        Returns
        -------
        None

        """
        self.logger.info('Monitoring new USB devices...')
        context = pyudev.Context()
        usb_monitor = pyudev.Monitor.from_netlink(context)

        udev_device: pyudev.Device
        for udev_device in iter(usb_monitor.poll, None):
            # device is added -> evaluate whether to authorize its interfaces
            if udev_device.device_type == 'usb_device' and udev_device.action == 'add':
                device = USBDevice(udev_device)
                self.logger.debug('Found new device %s at %s',
                                  device.identifier,
                                  device.udev_dev.sys_path)
                self.sys_path_device_id_map[device.udev_dev.sys_path] = device.identifier
                device_status = self._evaluate_device(device)
                status = {
                    'status': device_status,
                    'name': device.name,
                    'identifier': device.identifier,
                    'sys_path': device.udev_dev.sys_path,
                    'capabilities': device.capabilities
                }
                IPCClient.update_status(status)
            elif udev_device.device_type == 'usb_device' and udev_device.action == 'remove':
                if udev_device.sys_path not in self.sys_path_device_id_map:
                    self.logger.debug('Device @ %s disconnected, but has never been (fully) '
                                      'authorized', udev_device.sys_path)
                    continue

                device_id = self.sys_path_device_id_map[udev_device.sys_path]
                del self.sys_path_device_id_map[udev_device.sys_path]
                status = {
                    'sys_path': udev_device.sys_path,
                    'identifier': device_id,
                    'status': DeviceStatus.DISCONNECTED.value
                }
                IPCClient.update_status(status)
                if device_id not in get_known_devices():
                    self.logger.debug('Device %s disconnected', device_id)
                    continue
                remove_from_known_devices(device_id)
                self.logger.debug('Device %s has been removed from system, removing from the list '
                                  'of known devices', device_id)

    def _evaluate_device(self, device: USBDevice) -> int:
        """Evaluate whether a newly connected USB device or one of its interfaces should be
        authorized.

        Parameters
        ----------
        device : USBDevice
            The USB device to evaluate.

        Returns
        -------
        int
            The authorization status according to the DeviceStatus Enum.

        """
        def has_keyboard(interfaces: List[USBInterface]) -> bool:
            for interface in interfaces:
                if interface.if_cls == '00':
                    parent = interface.parent
                    if parent.dev_cls == '03' and interface.if_proto == '01':
                        return True
                elif interface.if_cls == '03' and interface.if_proto == '01':
                    return True
            return False

        def has_mouse(interfaces: List[USBInterface]) -> bool:
            for interface in interfaces:
                if interface.if_cls == '00':
                    parent = interface.parent
                    if parent.dev_cls == '03' and interface.if_proto == '02':
                        return True
                elif interface.if_cls == '03' and interface.if_proto == '02':
                    return True
            return False

        def has_network(interfaces: List[USBInterface]) -> bool:
            for interface in interfaces:
                if interface.if_cls == '00':
                    parent = interface.parent
                    if (parent.dev_cls in ['02', '0A']
                            or parent.dev_cls == 'EF' and parent.dev_subcls == '04'):
                        return True
                elif (interface.if_cls in ['02', '0A']
                        or interface.if_cls == 'EF' and interface.if_subcls == '04'):
                    return True
            return False

        def has_vendor_specific(interfaces: List[USBInterface]) -> bool:
            for interface in interfaces:
                if interface.if_cls == '00':
                    parent = interface.parent
                    if parent.dev_cls in ['FF', '00']:
                        return True
                if interface.if_cls == 'FF':
                    return True
            return False

        status = DeviceStatus.BLOCKED.value  # default status: blocked

        # check allowlist
        if device.identifier in get_whitelist().keys():
            status = DeviceStatus.WHITELISTED.value
            self.logger.debug('New device %s matched whitelist', device.identifier)
            if device.authorize_interfaces():
                add_to_known_devices(device.identifier)
            return status

        # check special interfaces
        configuration: usb.core.Configuration = device.usblib_dev.get_active_configuration()
        pyusb_if: usb.core.Interface
        interfaces = []
        for pyusb_if in configuration.interfaces():
            interface = device.usbinterface_from_pyusb_interface(pyusb_if)
            if interface.if_cls == '09':
                # always auth hub interfaces silently
                interface.authorize()
                interface.probe()
            interfaces.append(interface)
        if device.is_full_authorized():
            # device only had hub interfaces
            add_to_known_devices(device.identifier)
            return DeviceStatus.TRUSTED.value

        if has_keyboard(interfaces):
            status = DeviceStatus.KEYBOARD_MODE.value
            input_ifs = []
            # auth and monitor all keyboard interfaces, collect other input interfaces
            # for some mice the mouse if will only work in combination with another 'generic' input
            # interface, so we also add those to the timed allow
            for interface in interfaces:
                if interface.if_cls == '00':
                    parent = interface.parent
                    if parent.dev_cls == '03':
                        input_ifs.append(interface)
                        if interface.if_proto == '01':
                            interface.authorize()
                            interface.probe()
                elif interface.if_cls == '03':
                    input_ifs.append(interface)
                    if interface.if_proto == '01':
                        interface.authorize()
                        interface.probe()
            KeyMonitor.limit_keys(device)
            if has_mouse(interfaces):
                self.logger.warning('New keyboard and mouse combo %s connected, authoziring and '
                                    'limiting keys for kb, timed allowing mouse', device.identifier)
                IPCClient.prompt_notification('new_keyboard_mouse', device)
                TimeoutManager.timed_allow(device, input_ifs)
            else:
                self.logger.warning('New keyboard %s connected, authorizing and limiting keys',
                                    device.identifier)
                IPCClient.prompt_notification('new_keyboard', device)
        elif has_mouse(interfaces):
            # we know there are no keyboard interfaces in this branch, so we want to timed allow all
            # HID interfaces
            input_ifs = []
            is_mouse = True
            for interface in interfaces:
                if interface.if_cls == '00':
                    parent = interface.parent
                    if parent.dev_cls == '03':
                        input_ifs.append(interface)
                    else:
                        is_mouse = False
                elif interface.if_cls == '03':
                    input_ifs.append(interface)
                else:
                    is_mouse = False
            if is_mouse:
                # device only has HID interfaces that are not keyboards, so we can safely allow all
                # of them automatically
                status = DeviceStatus.TRUSTED.value
                for interface in interfaces:
                    interface.authorize()
                    interface.probe()
                IPCClient.prompt_notification('new_auth', device)
            else:
                # device could potentially allow its other interfaces automatically with its mouse
                # interface, so we want to delay the mouse authorization
                IPCClient.prompt_notification('new_mouse_other', device)
                TimeoutManager.timed_allow(device, input_ifs)
        elif has_network(interfaces):
            status = DeviceStatus.BLOCKED.value
            self.logger.warning('New network device %s connected, not authorizing.',
                                device.identifier)
            IPCClient.prompt_notification('new_network', device)
        elif has_vendor_specific(interfaces):
            status = DeviceStatus.BLOCKED.value
            self.logger.warning('New device with unknown functionality %s connected, not '
                                'authorizing', device.identifier)
            IPCClient.prompt_notification('new_vendor_specific', device)
        else:
            # generic path, look for lockscreen and strictness mode
            if lockscreen_is_active():
                status = DeviceStatus.BLOCKED.value
                self.logger.warning('New device %s connected while the screen was locked, not'
                                    'authorizing', device.identifier)
                IPCClient.prompt_notification('blocked_locked', device)
            else:
                strictness_level = get_strictness_level()
                if strictness_level is StrictnessLevel.LOW:
                    status = DeviceStatus.TRUSTED.value
                    self.logger.info('New device %s connected, authorizing', device.identifier)
                    device.authorize_interfaces()
                elif strictness_level is StrictnessLevel.MEDIUM:
                    status = DeviceStatus.TRUSTED.value
                    self.logger.info('New device %s connected, authorizing', device.identifier)
                    device.authorize_interfaces()
                    IPCClient.prompt_notification('new_auth', device)
                elif strictness_level is StrictnessLevel.HIGH:
                    status = DeviceStatus.BLOCKED.value
                    self.logger.info('New device %s connected, waiting for manual authorization',
                                     device.identifier)
                    IPCClient.prompt_notification('new_block', device)
        if device.is_full_authorized():
            self.logger.debug('All interfaces for device %s have been authorized, adding to list '
                              'of known devices', device.identifier)
            add_to_known_devices(device.identifier)
        return status

    def _init_known_devices(self) -> None:
        """Write all currently connected and fully authorized USB devices to the list of known
        devices.

        A USB device counts as ully authorized, if the device is authorized, and all of its
        immediate interfaces are authorized.

        Parameters
        ----------

        Returns
        -------
        None

        """
        if not path.isfile(KNOWN_DEVICE_PATH):
            os.mknod(KNOWN_DEVICE_PATH)
            os.chmod(KNOWN_DEVICE_PATH, 0o666)
        self.logger.info('Adding all currently authorized devices to the list of known devices')
        context = pyudev.Context()
        known_devices: List[str] = []

        udev_device: pyudev.Device
        for udev_device in context.list_devices(subsystem='usb'):
            if udev_device.device_type != 'usb_device':
                continue
            device = USBDevice(udev_device)
            if not device.is_full_authorized():
                continue
            self.logger.debug('Adding device %s @ %s to the list of known devices',
                              device.identifier, device.udev_dev.sys_path)
            known_devices.append(device.identifier)
            self.sys_path_device_id_map[device.udev_dev.sys_path] = device.identifier

        save_known_devices(known_devices)
        self.logger.info('Known device initialization completed')


def remove_from_known_devices(device_id: str) -> None:
    """Remove a device from the list of known devices

    Parameters
    ----------
    device_id : str
        The device ID of the device to remove from the known devices.

    Returns
    -------
    None

    """
    with open(KNOWN_DEVICE_PATH, 'r') as stream:
        known_devices: List[str] = yaml.safe_load(stream)
    if device_id in known_devices:
        known_devices.remove(device_id)
        with UIDLock(0):
            with open(KNOWN_DEVICE_PATH, 'w') as stream:
                yaml.safe_dump(known_devices, stream)


def add_to_known_devices(device_id: str) -> None:
    """Add a device to the list of known devices

    Parameters
    ----------
    device_id : str
        The device ID  of the device to add to the known devices.

    Returns
    -------
    None

    """
    with open(KNOWN_DEVICE_PATH, 'r') as stream:
        known_devices: List[str] = yaml.safe_load(stream)
    if device_id not in known_devices:
        known_devices.append(device_id)
        with UIDLock(0):
            with open(KNOWN_DEVICE_PATH, 'w') as stream:
                yaml.safe_dump(known_devices, stream)


def remove_from_whitelist(identifier: str) -> None:
    """Remove a device from the whitelist.

    Parameters
    ----------
    identifier : str
        The identifier of the device to remove from the whitelist.

    Returns
    -------
    None

    """
    with open(WHITELIST_PATH, 'r') as stream:
        whitelist = yaml.safe_load(stream)
    if identifier in whitelist:
        del whitelist[identifier]
        with UIDLock(0):
            with open(WHITELIST_PATH, 'w') as stream:
                yaml.dump(whitelist, stream)


def add_to_whitelist(identifier: str, name: str) -> None:
    """Add a device to the whitelist.

    Parameters
    ----------
    identifier : str
        The identifier of the device to add to the whitelist.
    name : str
        The device name of the device to add to the whitelist.

    Returns
    -------
    None

    """
    with open(WHITELIST_PATH, 'r') as stream:
        whitelist = yaml.safe_load(stream)
    if identifier not in whitelist:
        whitelist[identifier] = name
        with UIDLock(0):
            with open(WHITELIST_PATH, 'w') as stream:
                yaml.dump(whitelist, stream)


def remove_from_keyboard_mode(identifier: str) -> None:
    """Remove a device from the keyboard mode

    Parameters
    ----------
    identifier : str
        The device identifier of the device to remove from the keyboard mode.

    Returns
    -------
    None

    """
    with open(KEYBOARD_MODE_PATH, 'r') as stream:
        kb_mode_devs: List[str] = yaml.safe_load(stream)
    if identifier in kb_mode_devs:
        kb_mode_devs.remove(identifier)
        with UIDLock(0):
            with open(KEYBOARD_MODE_PATH, 'w') as stream:
                yaml.dump(kb_mode_devs, stream)


def get_known_devices() -> List[str]:
    """Get a list of known devices.

    Parameters
    ----------

    Returns
    -------
    List[str]
        The list of known devices.

    """
    with open(KNOWN_DEVICE_PATH, 'r') as stream:
        known_devices = yaml.safe_load(stream)
    return known_devices


def save_known_devices(known_devices: List[str]) -> None:
    """Save the known devices list.

    Parameters
    ----------
    known_devices : List[str]
        The known devices list.

    Returns
    -------
    None

    """
    with UIDLock(0):
        with open(KNOWN_DEVICE_PATH, 'w') as stream:
            yaml.safe_dump(known_devices, stream)


def get_whitelist() -> Dict[str, str]:
    """Get the whitelisted devices.

    Parameters
    ----------

    Returns
    -------
    Dict[str, str]
        A mapping of the short ID of a whitelisted device, to its name.

    """
    with open(WHITELIST_PATH, 'r') as stream:
        whitelist = yaml.safe_load(stream)
    return whitelist


def get_strictness_level() -> StrictnessLevel:
    """Get the currently active strictness level.

    Parameters
    ----------

    Returns
    -------
    StrictnessLevel
        The currently active strictness level.

    """
    with open(STRICTNESS_LEVEL_PATH, 'r') as stream:
        strictness_level = StrictnessLevel(yaml.safe_load(stream))
    return strictness_level


def get_active_users() -> Set[int]:
    """Get all users that currently have an active session.

    Parameters
    ----------

    Returns
    -------
    Set[int]
        A set of userids of all the users that have an active session.

    """
    active_users = set()
    completed = subprocess.run(['loginctl', 'list-sessions'], capture_output=True, check=True)
    lines = [line.strip() for line in completed.stdout.decode('utf-8').split('\n')][1:]
    for line in lines:
        if line == '':
            break
        _, uid, user, *args = line.split(' ')
        if not args:
            # no seat and no tty
            continue
        if not args[0].startswith('seat'):
            # no seat
            continue
        if user == 'gdm':
            # gdm is not a real user
            continue
        active_users.add(int(uid))
    return active_users


def lockscreen_is_active() -> bool:
    """Check whether all logged in users currently have their lockscreens active.

    Parameters
    ----------

    Returns
    -------
    bool
        Returns True if the lockscreens of *all* logged in users are currently active, else False.

    """
    for uid in get_active_users():
        ROOT_LOGGER.debug('Checking for active lockscreen of uid %d', uid)
        os.environ['DBUS_SESSION_BUS_ADDRESS'] = f'unix:path=/run/user/{uid}/bus'
        try:
            with UIDLock(uid):
                bus = SessionBus()
            ssaver = bus.get('org.gnome.ScreenSaver', '/org/gnome/ScreenSaver')
        except Exception as exc:
            ROOT_LOGGER.warning('Could not get lockscreen status of user %d: %s',
                                uid, exc)
            continue
        if not ssaver.GetActive():
            ROOT_LOGGER.debug('User with uid %d has an unlocked session!', uid)
            return False
    ROOT_LOGGER.debug('All active user sessions are locked')
    return True


def ensure_root() -> None:
    """Stop the daemon if it's not started by the root user

    Parameters
    ----------

    Returns
    -------
    None

    """
    if not os.getuid() == 0:
        ROOT_LOGGER.critical('Not running as root - UID is %d', os.getuid())
        print('This daemon must be run as root!')
        sys.exit(1)
    ROOT_LOGGER.debug('Running as root')


def set_up_logger() -> None:
    """Set up the logger format and loglevel.

    Parameters
    ----------

    Returns
    -------
    None

    """
    formatter = logging.Formatter('%(asctime)s [%(name)-18.18s] [%(levelname)-8.8s]  %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    ROOT_LOGGER.addHandler(console_handler)
    ROOT_LOGGER.setLevel(LOGLEVEL)


def restore_automatic_auth() -> None:
    """Restore the automatic authorization of USB interfaces.

    Also authorizes all plugged in interfaces.

    Parameters
    ----------

    Returns
    -------
    None

    """
    usb_bus: str
    for usb_bus in os.listdir('/sys/bus/usb/devices/'):
        if usb_bus.startswith('usb'):
            bus_path = path.join('/sys/bus/usb/devices/', usb_bus)
            if_auth_file = path.join(bus_path, 'interface_authorized_default')
            auth_file = path.join(bus_path, 'authorized')
            os.seteuid(0)  # loop should have stopped at this point -> no need for UIDLock
            with open(if_auth_file, 'w') as stream:
                stream.write('1')
            with open(auth_file, 'w') as stream:
                stream.write('0')
            time.sleep(0.1)
            with open(auth_file, 'w') as stream:
                stream.write('1')
    ROOT_LOGGER.warning('Automatic authorization restored.')


def capture_sigterm() -> None:
    """Restores the automatic authorization when the process is killed through SIGTERM.

    Parameters
    ----------

    Returns
    -------
    None

    """
    def handle_sigterm(*_args: Any) -> None:
        restore_automatic_auth()
        sys.exit(0)
    signal.signal(signal.SIGTERM, handle_sigterm)


def main() -> None:
    set_up_logger()
    ensure_root()
    capture_sigterm()

    ipc_server = IPCServer()
    ipc_server.listen()

    usb_protector = USBProtectorDaemon()
    try:
        usb_protector.run()
    except Exception as e:
        ROOT_LOGGER.critical(traceback.format_exc())
        restore_automatic_auth()
        sys.exit(1)


if __name__ == '__main__':
    main()
