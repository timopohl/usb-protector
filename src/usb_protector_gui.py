#!/usr/bin/env python3

from __future__ import annotations
import json
import os
import sys
import gettext
import enum
from typing import Any, List, Dict, Tuple
from threading import Thread

import gi
import yaml
import zmq
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib

LANGUAGE = os.environ['LANG'].split('.')[0]
if LANGUAGE not in ['de_DE', 'en_US']:
    LANGUAGE = 'en_US'
LOCALE_PATH = os.path.join(os.path.dirname(__file__), '..', 'data', 'locale')
TRANSLATION = gettext.translation('usb_protector_gui', LOCALE_PATH, languages=[LANGUAGE], fallback=True)
_ = TRANSLATION.gettext

WHITELIST_PATH = '/etc/usb_protector/whitelist.yml'
KNOWN_DEVICES_PATH = '/etc/usb_protector/known_devices.yml'
KEYBOARD_MODE_PATH = '/etc/usb_protector/keyboard_mode.yml'
STRICTESS_MODE_PATH = '/etc/usb_protector/strictness_level.yml'
CONFIG_PATH = '/etc/usb_protector/zmq_config.yml'
with open(f'/etc/usb_protector/usb_capabilities_{LANGUAGE}.yml', 'r') as stream:
    USB_CAPABILITIES = yaml.safe_load(stream)


whitelisted_components = {}


# TODO: create files, if they dont exist


class DeviceStatus(enum.Enum):
    """Positive values are determined by the oder of the combobox buttons for the device combobox!
    """
    NOOP = -2
    DISCONNECTED = -1
    WHITELISTED = 0
    TRUSTED = 1
    KEYBOARD_MODE = 2
    KEYBOARD_MOUSE_MODE = 3
    BLOCKED = 4


class UpdateServer:
    """The server that listens to device status updates

    Attributes
    ----------
    prefix : str
        The message prefix for messages that are meant for the GUI
    socket : zmq.Socket
        The socket that the server is listening on
    active_components : Dict[str, Any]
        A Dict containing a mapping of sys_paths to Gtk.ListBoxRows for all active devices.
    active_listbox : Gtk.ListBox
        The ListBox containing all the currently active devices
    whitelist_listbox : Gtk.ListBox
        The ListBox containing all the currently whitelisted devices
    """

    def __init__(self,
                 port: int,
                 prefix: str,
                 active_listbox: Gtk.ListBox,
                 whitelist_listbox: Gtk.ListBox,
                 active_components: Dict[str, Any]) -> None:
        context = zmq.Context.instance()
        self.prefix = prefix
        self.socket = context.socket(zmq.SUB)
        self.socket.connect(f'tcp://localhost:{port}')
        self.socket.setsockopt(zmq.SUBSCRIBE, prefix.encode('utf-8'))
        self.active_components = active_components
        self.active_listbox = active_listbox
        self.whitelist_listbox = whitelist_listbox

    def run(self):
        """Start the Update Server.
        """
        thread = Thread(target=self._run, daemon=True)
        thread.start()

    def _run(self):
        """Listen to new messages on the socket.
        """
        while True:
            msg = self.socket.recv().decode('utf-8')
            msg = msg[len(self.prefix):]
            update = yaml.safe_load(msg)
            status = DeviceStatus(update['status'])
            if status is DeviceStatus.NOOP:
                pass
            elif status is DeviceStatus.DISCONNECTED:
                GLib.idle_add(self._handle_disconnect, update)
            else:
                GLib.idle_add(self._handle_status_update, update)

    def _handle_disconnect(self, update: Dict):
        """Remove a disconnected device from all relevant GUI elements.

        Parameters
        ----------
        update : Dict
            A dict containing data about the device status update.
        """
        global whitelisted_components
        dev_path = update['sys_path']
        identifier = update['identifier']
        if dev_path in self.active_components:
            row = self.active_components[dev_path]['row']
            self.active_listbox.remove(row)
            del self.active_components[dev_path]
        if identifier not in whitelisted_components:
            with open(WHITELIST_PATH, 'r') as stream:
                whitelisted_devices = yaml.safe_load(stream)
            if identifier in whitelisted_devices.keys():
                components = add_whitelist_row(self.whitelist_listbox, identifier, whitelisted_devices[identifier])
                whitelisted_components[identifier] = components

    def _handle_status_update(self, update: Dict):
        """Update all relevant GUI elements for a device.

        Parameters
        ----------
        update : Dict
            A dict containing data about the device status update.
        """
        global whitelisted_components
        dev_path = update['sys_path']
        identifier = update['identifier']
        if dev_path in self.active_components:
            # device is already connected, just needs status update
            combobox = self.active_components[dev_path]['combobox']
            combobox.set_property('active', update['status'])
        else:
            # device is not connected -> new row
            components = add_device_row(self.active_listbox, update)
            self.active_components[dev_path] = components
            if identifier in whitelisted_components:
                # device was whitelisted -> remove row
                row = whitelisted_components[identifier]['row']
                self.whitelist_listbox.remove(row)
                del whitelisted_components[identifier]


class Requestor:
    """Class to send requests to the usb protector daemon.

    Attributes
    ----------
    context : zmq.Context
        Context to get sockets from.
    socket : zmq.Socket
        The socket used to receive requests and respond.
    zmq_config : Dict
        The config for the socket connection.
    """

    context = zmq.Context.instance()
    socket = context.socket(zmq.REQ)
    with open(CONFIG_PATH, 'r') as stream:
        zmq_config = yaml.safe_load(stream)

    @classmethod
    def list_devices(cls) -> List[Dict[str, str]]:
        """Get a list of all currently connected USB devices.

        Parameters
        ----------

        Returns
        -------
        List[Dict[str, str]]
            List of all currently connected USB devices.

        """
        # relevant info:
        # sys_path
        # name
        # identifier (long)
        cls.socket.connect(f'tcp://localhost:{cls.zmq_config["SERVER_REP_PORT"]}')
        cls.socket.send('LIST_DEVICES'.encode('utf-8'))
        resp = cls.socket.recv().decode('utf-8')
        devices = yaml.safe_load(resp)
        return devices



class Notificator:
    """Class to send device status updates to the USB Protector daemon.

    Attributes
    ----------
    context : zmq.Context
        Context to get sockets from.
    socket : zmq.Socket
        The socket used to send the updates.
    """

    context = zmq.Context.instance()
    socket = context.socket(zmq.PUSH)
    with open(CONFIG_PATH, 'r') as stream:
        zmq_config = yaml.safe_load(stream)

    @classmethod
    def notify_daemon(cls, msg: str):
        """Update the USB Protector daemon about a device status update.

        Parameters
        ----------
        msg : str
            The update as a YAML string.
        """
        print('notifying daemon: ', repr(msg))
        cls.socket.connect(f'tcp://localhost:{cls.zmq_config["SERVER_RECV_PORT"]}')
        cls.socket.send(f'GUI{msg}'.encode('utf-8'))


class Handler:
    """Handles signals of the GUI main window.
    """

    def on_destroy(self, *args):
        """Handler for the destroy event (window is closed)

        Parameters
        ----------
        args : Any
            Additional arguments.
        """
        Gtk.main_quit()

    def strictness_mode_changed(self, combobox):
        """Handler for the change of the strictnes mode.

        Parameters
        ----------
        combobox : Gtx.ComboBoxText
            The combobox thats used to change the strictness mode.
        """
        new_mode = combobox.get_active()
        with open(STRICTESS_MODE_PATH, 'w') as stream:
            yaml.safe_dump(new_mode, stream)


def add_device_row(listbox: Gtk.ListBox, device: Dict[str, str]) -> Dict:
    """Add a new device to the gui.

    In particular adds a new row to the ListBox that contains all the connected devices.

    Parameters
    ----------
    listbox : Gtk.ListBox
        The ListBox containing all the connected devices.
    device : Dict[str, str]
        Dictionary containing the device data.

    Returns
    -------
    Dict
        Dictionary containing the newly created Gtk.ListBoxRow of the device, and the
        Gtk.ComboBoxText that is used to change the device state.

    """
    row = Gtk.ListBoxRow()
    row.set_property('name', f'listbox1row{device["identifier"]}')
    row.set_property('width_request', 100)
    row.set_property('height_request', 20)
    row.set_property('visible', True)
    row.set_property('can_focus', True)

    row_box = Gtk.Box()
    row_box.set_property('visible', True)
    row_box.set_property('can_focus', False)
    row_box.set_property('orientation', Gtk.Orientation.VERTICAL)

    device_box = Gtk.Box()
    device_box.set_property('visible', True)
    device_box.set_property('can_focus', False)
    device_box.set_property('spacing', 15)
    device_box.set_property('homogeneous', True)

    device_label = Gtk.Label()
    device_label.set_label(device["name"])
    device_label.set_property('visible', True)
    device_label.set_property('can_focus', False)
    device_label.set_property('margin_right', 0)
    device_label.set_property('xalign', 0)

    capabilities = translate_capabilities(device['capabilities'])
    capability_label = Gtk.Label()
    capability_label.set_label(_('Capabilities: ') + '<b>' + ', '.join(capabilities) + '</b>')
    capability_label.set_property('visible', True)
    capability_label.set_property('use_markup', True)
    capability_label.set_property('can_focus', False)
    capability_label.set_property('margin_right', 0)
    capability_label.set_property('xalign', 0)

    device_combobox = Gtk.ComboBoxText()
    device_combobox.set_property('name', f'dev{device["identifier"]}combobox')
    device_combobox.set_property('visible', True)
    device_combobox.set_property('can_focus', False)
    device_combobox.set_property('halign', Gtk.Align.END)
    device_combobox.set_property('valign', Gtk.Align.END)
    device_combobox.append_text(_('Whitelisted'))
    device_combobox.append_text(_('Trusted'))
    device_combobox.append_text(_('Keyboard-mode'))
    device_combobox.append_text(_('Keyboard-mouse-mode'))
    device_combobox.append_text(_('Blocked'))
    status = device['status']
    device_combobox.set_property('active', status)
    device_combobox.connect('changed', lambda w: handle_state_change(w, device['sys_path']))

    renderer = device_combobox.get_cells()[0]
    model = device_combobox.get_model()
    device_combobox.set_cell_data_func(renderer, set_sensitive, model)

    device_box.add(device_label)
    device_box.add(capability_label)
    device_box.add(device_combobox)
    row_box.add(device_box)
    row.add(row_box)
    listbox.prepend(row)

    components = {
        'row': row,
        'combobox': device_combobox
    }
    return components


def add_whitelist_row(whitelist_listbox: Gtk.ListBox, device_id: str, device_name: str) -> Dict:
    """Adds a new device to the list of disconnected but whitelisted devices.

    Parameters
    ----------
    whitelist_listbox : Gtk.ListBox
        The ListBox containing all the whitelisted but disconnected devices
    device_id : str
        The ID of the device
    device_name :
        The name of the device

    Returns
    -------
    Dict
        Dictionary containing the Gtk.ListBoxRow within the whitelist_listbox representing the
        device

    """
    whitelist_row = Gtk.ListBoxRow()
    whitelist_row.set_property('name', f'whitelistListbox{device_id}')
    whitelist_row.set_property('width_request', 100)
    whitelist_row.set_property('height_request', 20)
    whitelist_row.set_property('visible', True)
    whitelist_row.set_property('can_focus', True)

    whitelist_row_box = Gtk.Box()
    whitelist_row_box.set_property('visible', True)
    whitelist_row_box.set_property('can_focus', False)
    whitelist_row_box.set_property('orientation', Gtk.Orientation.VERTICAL)

    whitelist_device_box = Gtk.Box()
    whitelist_device_box.set_property('visible', True)
    whitelist_device_box.set_property('can_focus', False)
    whitelist_device_box.set_property('spacing', 32)
    whitelist_device_box.set_property('homogeneous', True)

    whitelist_device_label = Gtk.Label()
    whitelist_device_label.set_label(device_name)
    whitelist_device_label.set_property('visible', True)
    whitelist_device_label.set_property('can_focus', False)
    whitelist_device_label.set_property('margin_right', 122)
    whitelist_device_label.set_property('xalign', 0)

    whitelist_device_button = Gtk.Button()
    whitelist_device_button.set_property('label', _('Remove'))
    whitelist_device_button.set_property('visible', 'True')
    whitelist_device_button.set_property('can_focus', 'True')
    whitelist_device_button.set_property('receives_default', 'True')
    whitelist_device_button.set_property('halign', Gtk.Align.END)
    whitelist_device_button.set_property('valign', Gtk.Align.END)
    whitelist_device_button.connect('clicked', lambda w: handle_whitelist_remove(w,
                                                                                 whitelist_listbox,
                                                                                 whitelist_row,
                                                                                 device_id))
    whitelist_device_box.add(whitelist_device_label)
    whitelist_device_box.add(whitelist_device_button)
    whitelist_row_box.add(whitelist_device_box)
    whitelist_row.add(whitelist_row_box)
    whitelist_listbox.prepend(whitelist_row)

    components = {
        'row': whitelist_row
    }
    return components



def init_strictness_mode(builder: Gtk.Builder):
    """Display the currently active strictness mode.

    Parameters
    ----------
    builder : Gtk.Builder
        The Builder used to build the window.
    """
    strictness_mode_box: Gtk.ComboBoxText = builder.get_object('strictness_mode_box')
    with open(STRICTESS_MODE_PATH, 'r') as stream:
        strictness_mode = yaml.safe_load(stream)
    strictness_mode_box.set_active(strictness_mode)


def set_sensitive(combobox: Gtk.ComboBoxText,
                  cellrenderer: Gtk.CellRenderer,
                  liststore: Gtk.ListStore,
                  treeiter: Gtk.TreeIter,
                  treemodel: Gtk.TreeModel):
    """Sets the sensitivity ("clickability") of combobox entries for the device states.

    The user is not supposed to manually set a device to Keyboard- or Keyboard+Mouse-Mode.
    Therefore, these two options should be unclickable, unless the mode is already active (so you
    don't lose access to it just by clicking on the combobox itself).

    Parameters
    ----------
    combobox : Gtk.ComboBoxText
        The ComboBox containing the states of a connected device
    cellrenderer : Gtk.CellRenderer
        The CellRenderer of the combobox
    liststore : Gtk.ListStore
        The ListStore of the combobox (required by set_sensitive spec)
    treeiter : Gtk.TreeIter
        The TreeIter of the combobox (required by set_sensitive spec)
    treemodel :
        The TreeModel of the combobox
    """
    index = treemodel.get_path(treeiter)
    if index[0] not in [DeviceStatus.KEYBOARD_MOUSE_MODE.value, DeviceStatus.KEYBOARD_MODE.value]:
        sensitive = True
    else:
        sensitive = index[0] == combobox.get_active()
    cellrenderer.set_sensitive(sensitive)


def handle_whitelist_remove(button: gtk.Button,
                            whitelist_listbox: gtk.ListBox,
                            whitelist_row: gtk.ListBoxRow,
                            device_id: str):
    """Handle the removal of a *disconnected* device from the whitelist through a click on the
    'Remove' button

    Parameters
    ----------
    button : gtk.Button
        The Button that was used to trigger the function.
    whitelist_listbox : gtk.ListBox
        The ListBox for disconnected whitelisted devices.
    whitelist_row : gtk.ListBoxRow
        The ListBoxRow containing the removed device.
    device_id : str
        The id of the removed device.
    """
    global whitelisted_components
    # 1. notify daemon to remove from whitelist
    request = {
        'identifier': device_id,
        'action': DeviceStatus.DISCONNECTED.value
    }
    msg = yaml.dump(request)
    Notificator.notify_daemon(msg)

    # 2. remove whitelist_row from whitelist_listbox
    whitelist_listbox.remove(whitelist_row)
    del whitelisted_components[device_id]


def handle_state_change(combobox: Gtk.ComboBoxText, sys_path: str):
    """Handle the change of a connected devices state through the devices combobox.

    Parameters
    ----------
    combobox : Gtk.ComboBoxText
        The combobox that was used to trigger the event.
    sys_path : str
        The sys path of the device.

    """
    request = {
        'action': combobox.get_active(),
        'sys_path': sys_path
    }
    msg = yaml.dump(request)
    Notificator.notify_daemon(msg)


def translate_capabilities(capability_ids: Set[str]) -> Set[str]:
    """'Translates' the capability IDs in more expressive, human readable descriptions of these
    capabilities.

    Parameters
    ----------
    capability_ids : Set[str]
        A set of the capability IDs to translate

    Returns
    -------
    Set[str]
        The set of descriptions for the capabilities contained in the capability_ids set.

    """
    capabilities = set()
    for capability_id in capability_ids:
        if_cls, subcls_id = capability_id.split('-')
        class_capabilities = USB_CAPABILITIES[if_cls]
        capabilities.add(class_capabilities.get(subcls_id, class_capabilities['default']))
    return capabilities


def main():
    global whitelisted_components
    # build main window
    builder = Gtk.Builder()
    builder.add_from_file(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', 'data',
                                       'ui',
                                       f'main_{LANGUAGE}.ui'))
    builder.connect_signals(Handler())
    active_listbox: Gtk.ListBox = builder.get_object('listbox1')

    # add active devices
    active_components = {}
    devices = Requestor.list_devices()
    for device in devices:
        active_components[device['sys_path']] = add_device_row(active_listbox, device)

    # add whitelisted devices
    whitelist_listbox: Gtk.ListBox = builder.get_object('whitelist_listbox')
    device_ids = [device['identifier'] for device in devices]
    with open(WHITELIST_PATH, 'r') as stream:
        whitelisted_devices = yaml.safe_load(stream)
    for device_id, device_name in whitelisted_devices.items():
        if device_id not in device_ids:
            whitelisted_components[device_id] = add_whitelist_row(whitelist_listbox, device_id, device_name)
    # make strictness mode work
    init_strictness_mode(builder)

    window: Gtk.Window = builder.get_object('main_window')
    window.show_all()
    # run update server
    with open(CONFIG_PATH, 'r') as stream:
        zmq_confg = yaml.safe_load(stream)
    updater = UpdateServer(zmq_confg['SERVER_SEND_PORT'],
                           zmq_confg['GUI_PREFIX'],
                           active_listbox,
                           whitelist_listbox,
                           active_components)
    updater.run()
    Gtk.main()


if __name__ == '__main__':
    main()
