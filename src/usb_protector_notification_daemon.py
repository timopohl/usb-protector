#!/usr/bin/env python3

import yaml
import os
import dbus
import logging
import subprocess
import gettext
from time import sleep
from threading import Thread
from typing import Any, Dict, List, Set

import notify2
import evdev
import zmq
from gi.repository import GLib


LANG = os.environ['LANG'].split('.')[0]
if LANG not in ['de_DE', 'en_US']:
    LANG = 'en_US'
LOCALE_PATH = os.path.join(os.path.dirname(__file__), '..', 'data', 'locale')
TRANSLATION = gettext.translation('usb_protector_notification_daemon', LOCALE_PATH, languages=[LANG], fallback=True)
_ = TRANSLATION.gettext

with open(f'/etc/usb_protector/usb_capabilities_{LANG}.yml' ,'r') as stream:
    USB_CAPABILITIES = yaml.safe_load(stream)
with open(f'/etc/usb_protector/notifications_{LANG}.yml', 'r') as stream:
    NOTIFICATION_CONTENTS = yaml.safe_load(stream)
with open('/etc/usb_protector/zmq_config.yml', 'r') as stream:
    ZMQ_CFG = yaml.safe_load(stream)


class IPCClient:
    """IPC Client to send responses of notifications to the usb protector daemon
    """

    logger = logging.getLogger('IPCClient')
    ctx = zmq.Context.instance()
    socket = ctx.socket(zmq.PUSH)
    socket.connect(f'tcp://localhost:{ZMQ_CFG["SERVER_RECV_PORT"]}')

    @classmethod
    def send_request(cls, action: str, device_sys_path: str):
        """Send the response of a notification to the usb protector daemon

        Parameters
        ----------
        action : str
            The response choice. One of [auth, deauth, noop]
        device_sys_path : str
            The sys path of the device that the notification belongs to
        """
        cls.logger.info('Sending request to %s device %s', action, device_sys_path)
        msg = yaml.safe_dump({'device': device_sys_path, 'action': action})
        msg = f'{ZMQ_CFG["NOTIFICATION_PREFIX"]}{msg}'
        cls.socket.send(msg.encode('utf-8'))


class Notificator:
    """Class to handle notification requests by the usb protector daemon and forward them to the
    users notification server
    """

    def __init__(self, device_sys_path: str):
        """Constructor method

        Parameters
        ----------
        device_sys_path : str
            Sys path of the device that the notification is about
        """
        self.loop = GLib.MainLoop()
        self.device_sys_path = device_sys_path
        self.logger = logging.getLogger('Notificator')

    def notify(self, notification_info: Dict):
        """Start a thread that notifies the user and waits for a response

        Parameters
        ----------
        notification_info : Dict
            All the information necessary to show the notification
        """
        thread = Thread(target=self._notify,
                        args=(notification_info,),
                        daemon=True)
        thread.start()

    def _notify(self, notification_info: Dict[str, Any]) -> None:
        """Notify the user about a new device and wait for a response

        Parameters
        ----------
        notification_info : Dict[str, Any]
            All the information necessary to show the notification

        Returns
        -------
        None

        """
        def close_callback(n: int):
            """Callback that is called when a notification is closed.

            Parameters
            ----------
            n : int
                The amount of notifications the notification server has received so far
            """
            self.logger.debug('Notification for device %s closed', self.device_sys_path)
            self.loop.quit()

        def action_callback(n: int, action: str):
            """Callback that is called when an action is performed on the notification

            Actions can be a button press, or closing the notification by clicking on its body.

            Parameters
            ----------
            n : int
                The amount of notifications the notification server has received so far
            action : str
                The performed action. One of [auth, deauth, noop]
            """
            if action == 'settings':
                subprocess.Popen(['/usr/bin/usb_protector_gui'])
            else:
                IPCClient.send_request(action, self.device_sys_path)
            self.loop.quit()

        notification_content = NOTIFICATION_CONTENTS[notification_info['notification_type']]
        dev_name = notification_info['device_name']
        dev_caps = translate_capabilities(notification_info['device_capabilities'])
        # TODO: provide scancodes, and locally translate key
        pressed_key = scancode_to_key(notification_info['hit_scancode'])

        title = notification_content['title'].format(dev_name=dev_name, dev_caps=dev_caps)
        text = notification_content['text'].format(dev_name=dev_name,
                                                   dev_caps=dev_caps,
                                                   pressed_key=pressed_key)
        notification = notify2.Notification(title, text, icon='security-high')
        notification.set_urgency(notification_info['urgency'])
        notification.set_timeout(notify2.EXPIRES_NEVER)
        notification.connect('closed', close_callback)
        for identifier, text in notification_content['actions'].items():
            notification.add_action(identifier, text, action_callback)
        notification.add_action('settings', _('Settings'), action_callback)
        notification.show()
        self.loop.run()


class NotificatorDaemon:
    """IPC Server listening for notification requests by the usb protector daemon
    """

    def __init__(self):
        """Constructor method
        """
        # waiting for the notification server to register with dbus
        initialized = False
        while not initialized:
            try:
                notify2.init(f'USB Protector Notification Daemon {os.geteuid()}', 'glib')
                initialized = True
            except dbus.exceptions.DBusException:
                sleep(5)
        context = zmq.Context.instance()
        self.prefix = ZMQ_CFG['NOTIFICATION_PREFIX']
        self.logger = logging.getLogger('NotificationDaemon')
        self.socket = context.socket(zmq.SUB)
        self.socket.connect(f'tcp://localhost:{ZMQ_CFG["SERVER_SEND_PORT"]}')
        self.socket.setsockopt(zmq.SUBSCRIBE, self.prefix.encode('utf-8'))

    def run(self):
        """Start listening for notification requests from the usb protector daemon
        """
        while True:
            msg = self.socket.recv().decode('utf-8')
            msg = msg[len(self.prefix):]
            notification_info = yaml.safe_load(msg)
            # notification_info contains the keys ['device', 'title', 'text', 'actions' 'urgency']
            notificator = Notificator(notification_info['device'])
            notificator.notify(notification_info)


def translate_capabilities(capability_ids: Set[str]) -> str:
    """Translate a set of capability ids to a set of human readable capabilities

    Capability ids are a combination of usb-class, -subclass and -protocol

    Parameters
    ----------
    capability_ids : Set[str]
        The capability IDs you want to translate

    Returns
    -------
    str
        A string containing a set of comma separated human readable capabilities

    """
    capabilities = set()
    for capability_id in capability_ids:
        if_cls, subcls_id = capability_id.split('-')
        class_capabilities = USB_CAPABILITIES[if_cls]
        capabilities.add(class_capabilities.get(subcls_id, class_capabilities['default']))
    return ', '.join(capabilities)


def scancode_to_key(scancode: int) -> str:
    """Translate a keyboards scancodes to the corresponding key

    Parameters
    ----------
    scancode : int
        The scancode to translate

    Returns
    -------
    str
        The corresponding key according to the usb spec

    """
    # TODO: use local scancodes file to translate to language specific keypress
    return evdev.ecodes.KEY.get(scancode, 'None')


def main() -> None:
    # In case the daemon starts before x is fully initialized
    if 'DISPLAY' not in os.environ.keys():
        os.environ['DISPLAY'] = ':0'
    notificator = NotificatorDaemon()
    notificator.run()


if __name__ == '__main__':
    main()
